$Directory = "\\<server>\<share>"
$ACL = Get-ACL "${Directory}"
$Admin = New-Object System.Security.Principal.NTAccount("Domain\Principal")
$FullAccessRuleRecursive = New-Object System.Security.AccessControl.FileSystemAccessRule($Admin, 'FullControl', 'ContainerInherit,ObjectInherit', "None", 'Allow')
$ACL.AddAccessRule($FullAccessRuleRecursive)
Set-ACL -Path "${Directory}" -AclObject $ACL
